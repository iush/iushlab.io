---
layout: post
title: Transformación Digital
cover: Digital_Transformation.png
date: 2018-10-23 12:00:00
categories: posts
excerpt_separator: <!--more-->
---
<font color="#336699">PROFUNDIZACION II</font>

## Transformación digital

La transformación digital reconstruye las dinámicas de las organizaciones para adaptarlas a las necesidades del presente y del futuro.

Applying GitLab workflow to technical documentation related to the project is a natural fit. Since GitLab has flexible review processes and prioritizes continuous integration (CI), you can apply those benefits to documentation reviews and builds.

GitLab is a great match for when developers are writing documentation in source control. GitLab works well when a project is so large or distributed that no one person can know enough to write the documentation for the project.

- [Why use Gitab/GitHub as a Content Management System?](https://justwriteclick.com/2015/12/17/why-use-github-as-a-content-management-system/)
- [Continuous integration and delivery for documentation](https://opensource.com/business/15/7/continuous-integration-and-continuous-delivery-documentation)

## Iniciativas:

- [Trabajo colaborativo](/profte12/trabajo-colaborativo)
- [Sistema de Reserva de Espacios Físicos](/badat11/reserva-espacion-fisicos)

<!--more-->
