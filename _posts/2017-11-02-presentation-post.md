---
layout: post
title: Library Information Technology
cover: coverU.jpg
date:   2017-11-01 12:00:00
categories: posts
---

## Biblioteca de respositorios de clase.

![Tablero](/images/board_icon.png)

[Tablero de actividades](https://gitlab.com/iush/iush.gitlab.io/-/boards)

Iniciativa que contribuye a la expansión de la investigación usando infraestructura de repositorios, que busca desarrollar liderazgo como tambien incentivar oportunidades colaborativas a través del campus IUSH.

## Comunidades en los Repositorios:

- [Arquitectura del Software](http://arsoft12.gitlab.io/)
  - [Documentos](http://arsoft12.gitlab.io/posts/)
  - [Presentaciones](http://arsoft12.gitlab.io/slide/)
- [Arquitectura del Hardware](http://arquh11.gitlab.io/)
  - [Documentos](http://arquh11.gitlab.io/posts/)
  - [Presentaciones](http://arquh11.gitlab.io/slide/)
- [Documentos primitivos:](http://pepitosoft.gitlab.io)
    - [IMPLEMENTANDO LABORATORIO INTEGRACION CONTINUA CON GITLAB.](http://pepitosoft.gitlab.io/jekyll/hwarch/posts/implementando-ci/)
    - [GENERAL DOCUMENT SOFTWARE ARCHITECTURE.](http://pepitosoft.gitlab.io/jekyll/hwarch/posts/software-architecture-main-doc/)
    - [GENERAL DOCUMENT IMAGE RECOGNITION.](http://pepitosoft.gitlab.io/jekyll/hwarch/posts/int-art-images-p/)
    - [GENERAL DOCUMENT LSTM NETWORKS.](http://pepitosoft.gitlab.io/jekyll/hwarch/posts/int-art-w/)
    - [EMPLOYING GIT ON THIS PROJECT.](http://pepitosoft.gitlab.io/posts/hwarch/employing_git_on_this_project/)
    - [TRABAJO GENERAL ARQUITECTURA DE HARDWARE.](http://pepitosoft.gitlab.io/jekyll/hwarch/posts/engineering-hardware-w/)
    - [GENERAL DOCUMENT SOFTWARE ENGINEERING.](http://pepitosoft.gitlab.io/jekyll/hwarch/posts/ingenieria-software-finalwork/)
