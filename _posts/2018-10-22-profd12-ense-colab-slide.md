---
layout: slide
title: Trabajo Colaborativo  
cover: darl_owl.jpg
date: 2018-10-22 2:00:00
categories: posts presentation
theme: black
transition: slide
excerpt_separator: <!--more-->
---
<font color="#336699">Trabajo Colaborativo</font>
<!--more-->
<section data-background="/images/bg.svg" data-markdown data-separator="^----" data-separator-vertical="^>>>>" >
<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/Readme.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/Fundamentos.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBDoing.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBinteracting.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBsharing.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBsearching.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBbeing.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/Sources.md %}

</section>
