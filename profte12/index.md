---
layout: post
title: Profundizacion 2
cover: aix.png
date:   2018-10-25 3:00:00
categories: posts
author: oscar_munoz
hidden: true
---
<font color="#336699">Why use GitLab for docs?</font>

## PROFUNDIZACION 2

Applying GitLab workflow to technical documentation related to the project is a natural fit. Since GitLab has flexible review processes and prioritizes continuous integration (CI), you can apply those benefits to documentation reviews and builds.

GitLab is a great match for when developers are writing documentation in source control. GitLab works well when a project is so large or distributed that no one person can know enough to write the documentation for the project.

- [Why use Gitab/GitHub as a Content Management System?](https://justwriteclick.com/2015/12/17/why-use-github-as-a-content-management-system/)
- [Continuous integration and delivery for documentation](https://opensource.com/business/15/7/continuous-integration-and-continuous-delivery-documentation)

<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/Readme.md %}
