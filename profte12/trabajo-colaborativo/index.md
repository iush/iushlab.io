---
layout: post
title: Transformación Digital y Trabajo Colaborativo
cover: darl_owl.jpg
date:  2018-10-25 3:00:00
categories: posts
author: oscar_munoz
hidden: true
---
<font color="#336699">PROFUNDIZACION II</font>
## Trabajo Colaborativo

El trabajo colaborativo es la columna vertebral del desarrollo de software, ademas de ser una forma efectiva y eficiente de desarrollar ideas.
<font color="#336699">Why use GitLab for docs?</font>


- [Why use Gitab/GitHub as a Content Management System?](https://justwriteclick.com/2015/12/17/why-use-github-as-a-content-management-system/)
- [Continuous integration and delivery for documentation](https://opensource.com/business/15/7/continuous-integration-and-continuous-delivery-documentation)

<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/Readme.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/Fundamentos.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBDoing.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBinteracting.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBsharing.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBsearching.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/LBbeing.md %}
{% remote_markdown https://gitlab.com/iush/profte12/trabajo-colaborativo/raw/master/Sources.md %}
