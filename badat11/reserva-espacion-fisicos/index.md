---
layout: post
title: Transformación Digital y Trabajo Colaborativo
cover: darl_owl.jpg
date:  2018-10-25 3:00:00
categories: posts
author: oscar_munoz
hidden: true
---
<font color="#336699">Bases de datos</font>

## Sistema de Espacios Educativos	


Indicadores sobre el uso de los espacios educativos, que permitan encontrar falencias y/o fortalezas ademas de tomar decisiones administrativas de manera informada.

<font color="#336699">Sistema de Espacios Educativos</font>

<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/iush/reserva-espacion-fisicos/raw/master/Readme.md %}
{% remote_markdown https://gitlab.com/iush/reserva-espacion-fisicos/raw/master/transaction.md %}
{% remote_markdown https://gitlab.com/iush/reserva-espacion-fisicos/raw/master/ERD.md %}
{% remote_markdown https://gitlab.com/iush/reserva-espacion-fisicos/raw/master/Sources.md %}
